fetch('data.json').then(data => data.json()).then(data => {
    matchesPlayedChart(data);
    matchesWonChart(data);
    extraRunsChart(data);
    econBowlersChart(data);
});




// Create the chart

function matchesPlayedChart(data) {

    var matchesPerYear = [];
    var matches = data["matchesPlayedPerTeam"];

    // for (var i in matches) {
    //     var obj = {};
    //     obj["name"] = i;
    //     obj["y"] = matches[i];
    //     matchesPerYear.push(obj);
    // }

    var matchesPerYear = Object.keys(matches).map(item => ({
        'name': item,
        'y' : matches[item]
    }))


    Highcharts.chart('matchesChart', {
        chart: {
            type: 'column'
        },
        title: {
            text: Object.keys(data)[0]
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Matches played'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        "series": [{
            "colorByPoint": true,
            "data": matchesPerYear
        }]

    });
}

function matchesWonChart(data) {

    var years = Object.keys(data["matchesWonPerTeam"]["Kolkata Knight Riders"]);

    var matchesData = data["matchesWonPerTeam"]
    var matchesWonPerYear = Object.keys(matchesData).map(item => ({
        'name' : item,
        'data' : Object.values(matchesData[item])
    }))

    Highcharts.chart('winCountChart', {
        chart: {
            type: 'bar'
        },
        title: {
            text: Object.keys(data)[1]
        },
        xAxis: {
            categories: years
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches won'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: matchesWonPerYear

    });

}

function extraRunsChart(data) {


    //var extraRunsForYear = [];
    var extraData = data["extraRunsConceededPerTeam"];


    var extraRunsForYear = Object.keys(extraData).map(item => ({
        'name' : item,
        'y' : extraData[item]
    }))

    Highcharts.chart('extraRuns-chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: Object.keys(data)[2] + ' 2016'
        },
        xAxis: {
            type: 'category'

        },
        yAxis: {
            title: {
                text: 'Extra runs'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        "series": [{
            "colorByPoint": true,
            "data": extraRunsForYear
        }]

    });
}


function econBowlersChart(data) {

    var econData = data["economicalBowlers"];

    var econChartData = Object.keys(econData).map(item => ({
        'name' : item,
        'y' : econData[item]
    }))

    Highcharts.chart('EconomyRateChart', {
        chart: {
            type: 'column'
        },
        title: {
            text: Object.keys(data)[3]
        },

        xAxis: {
            type: 'category'

        },
        yAxis: {
            title: {
                text: 'Economy Rate'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,

                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        "series": [{
            "colorByPoint": true,
            "data": econChartData
        }]

    });
}