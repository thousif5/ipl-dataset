import {
  getNoOfMatchesPlayed,
  getNoOfMatchesWonPerTeamPerYear,
  getExtraRunsPerTeamForYear,
  getEconomicalBowlersForYear
} from "./ipl";

describe("IPL module", () => {
  describe("No. of matches played per team for all years, getNoOfMatchesPlayed", () => {
    const matchesSample = [
      {
        season: 2008,
      },
      {
        season: 2009
      },
      {
        season: 2008
      }
    ];
    const expectedResult = {
      2008: 2,
      2009: 1
    };
    test("should exist", () => {
      expect(getNoOfMatchesPlayed).toBeDefined();
    });
    test("should return an object", () => {
      expect(getNoOfMatchesPlayed(matchesSample)).toBeDefined();
      expect(typeof getNoOfMatchesPlayed(matchesSample)).toEqual("object");
      expect(getNoOfMatchesPlayed(matchesSample)).toEqual(expectedResult);
    });
  });
  describe("No. of matches won per team per year, getNoOfMatchesWonPerTeamPerYear", () => {

    const matchesWonSample = [
      {
        'season': '2008',
        'winner': 'Kolkata Knight Riders'
      },
      {
        'season': '2009',
        'winner': 'Deccan Chargers'
      }
    ];
    const expectedWonResult = {
      'Kolkata Knight Riders': {
        '2008' : 1
      },
      'Deccan Chargers': {
        '2009' : 1
      }
    };

    test("should exist", () => {
      expect(getNoOfMatchesWonPerTeamPerYear).toBeDefined();
    });
    test("should return an object", () => {
      expect(getNoOfMatchesWonPerTeamPerYear(matchesWonSample)).toBeDefined();
      expect(typeof getNoOfMatchesWonPerTeamPerYear(matchesWonSample)).toEqual("object");
      expect(getNoOfMatchesWonPerTeamPerYear(matchesWonSample)).toEqual(expectedWonResult);
    });
  });
  describe("Extra runs conceeded per team for year, getExtraRunsPerTeamForYear", () => {

    const sampleMatch = [
      {
        'id': '385',
        'season' : '2016'
      },
      {
        'id': '395',
        'season' : '2016'
      },
      {
        'id': '405',
        'season' : '2016'
      },
      {
        'id': '415',
        'season' : '2016'
      },
    ];

    const deliveriesSample = [
      {
        'match_id': '385',
        'bowling_team': 'KKR',
        'extra_runs': '4'
      },
      {
        'match_id': '395',
        'bowling_team': 'MI',
        'extra_runs': '4'
      },
      {
        'match_id': '405',
        'bowling_team': 'RCB',
        'extra_runs': '4'
      },
      {
        'match_id': '415',
        'bowling_team': 'DC',
        'extra_runs': '4'
      },
    ];
    const expectedRunsResult = {
      'KKR': 4,
      'MI' : 4,
      'RCB': 4,
      'DC': 4
    }

    test("should exist", () => {
      expect(getExtraRunsPerTeamForYear).toBeDefined();
    });
    test("should return an object", () => {
      expect(getExtraRunsPerTeamForYear(sampleMatch,deliveriesSample)).toBeDefined();
      expect(typeof getExtraRunsPerTeamForYear(sampleMatch,deliveriesSample)).toEqual("object");
      expect(getExtraRunsPerTeamForYear(sampleMatch,deliveriesSample)).toEqual(expectedRunsResult);
    });
  });
  describe("Economical bowlers for year, getEconomicalBowlersForYear", () => {

    const sampleM = [
      {
        'id': '385',
        'season' : '2015'
      },
      {
        'id': '395',
        'season' : '2015'
      },
      {
        'id': '405',
        'season' : '2015'
      },
      {
        'id': '415',
        'season' : '2015'
      },
    ];

    const sampleD = [
      {
        'match_id': '385',
        'bowler': 'Yadav',
        'total_runs': '6',
        'legbye_runs': '0',
        'wide_runs': '0',
        'noball_runs': '0',
        'bye_runs': '0',
        'penalty_runs': '0',
        'ball': '1'
      },
      {
        'match_id': '385',
        'bowler': 'Yadav',
        'total_runs': '2',
        'legbye_runs': '0',
        'wide_runs': '0',
        'noball_runs': '0',
        'bye_runs': '0',
        'penalty_runs': '0',
        'ball': '2'
      },
      {
        'match_id': '385',
        'bowler': 'Yadav',
        'total_runs': '1',
        'legbye_runs': '0',
        'wide_runs': '0',
        'noball_runs': '0',
        'bye_runs': '0',
        'penalty_runs': '0',
        'ball': '3'
      },
      {
        'match_id': '385',
        'bowler': 'Yadav',
        'total_runs': '3',
        'legbye_runs': '0',
        'wide_runs': '0',
        'noball_runs': '0',
        'bye_runs': '0',
        'penalty_runs': '0',
        'ball': '4'
      },
      {
        'match_id': '385',
        'bowler': 'Yadav',
        'total_runs': '4',
        'legbye_runs': '0',
        'wide_runs': '0',
        'noball_runs': '0',
        'bye_runs': '0',
        'penalty_runs': '0',
        'ball': '5'
      },
      {
        'match_id': '385',
        'bowler': 'Yadav',
        'total_runs': '2',
        'legbye_runs': '0',
        'wide_runs': '0',
        'noball_runs': '0',
        'bye_runs': '0',
        'penalty_runs': '0',
        'ball': '6'
      },
      // {
      //   'match_id': '395',
      //   'bowler': 'Jadav',
      //   'total_runs': '4',
      //   'legbye_runs': '0',
      //   'bye_runs': '0',
      //   'penalty_runs': '0',
      //   'ball': '1'
      // },
      // {
      //   'match_id': '405',
      //   'bowler': 'Madhav',
      //   'total_runs': '13',
      //   'legbye_runs': '0',
      //   'bye_runs': '0',
      //   'penalty_runs': '0',
      //   'ball': '1'
      // },
      // {
      //   'match_id': '415',
      //   'bowler': 'Kumar',
      //   'total_runs': '14',
      //   'legbye_runs': '0',
      //   'bye_runs': '0',
      //   'penalty_runs': '0',
      //   'ball': '1'
      // }
    ];

    const expectedEconResult = {
      Yadav: '18.00'
      // Jadav: 4,
      // Yadav: 10,
      // Madhav: 13,
      // Kumar: 14
    };
    
    test("should exist", () => {
      expect(getEconomicalBowlersForYear).toBeDefined();
    });
    test("should return an object", () => {
      expect(getEconomicalBowlersForYear(sampleM,sampleD)).toBeDefined();
      expect(typeof getEconomicalBowlersForYear(sampleM,sampleD)).toEqual("object");
      expect(getEconomicalBowlersForYear(sampleM,sampleD)).toEqual(expectedEconResult);
    });
  });
});
