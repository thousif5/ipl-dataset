const fs = require('fs');
const csv = require("csvtojson");
csv()
  .fromFile("matches.csv")
  .then(matches => {
    csv()
      .fromFile("deliveries.csv")
      .then(deliveries => {
        //forHyderabad(deliveries);
        let dataObj = {
          'matchesPlayedPerTeam': getNoOfMatchesPlayed(matches),
          'matchesWonPerTeam': getNoOfMatchesWonPerTeamPerYear(matches),
          'extraRunsConceededPerTeam': getExtraRunsPerTeamForYear(matches, deliveries),
          'economicalBowlers': getEconomicalBowlersForYear(matches, deliveries)

        }


        fs.writeFile('data.json', JSON.stringify(dataObj), 'utf8', function (err) {
          if (err) {
            return console.log(err);
          }
        })
      });
  });

// Q1
const getNoOfMatchesPlayed = (matches) => {

  var matchesObj = matches.reduce((acc, val) => {
    (acc.hasOwnProperty(val.season)) ? acc[val.season]++: acc[val.season] = 1;
    return acc;
  }, {})

  return matchesObj;


};

// Q2

const getNoOfMatchesWonPerTeamPerYear = (matches) => {
  //var matchesWon = {};
  var matchesWon = matches.reduce((acc, val) => {

    if (val['winner'] !== "") {
      if (acc.hasOwnProperty(val['winner'])) {

        if (acc[val['winner']].hasOwnProperty(val.season)) {
          acc[val.winner][val.season]++;
        } else {
          acc[val.winner][val.season] = 1;
        }
      } else {
        acc[val.winner] = {};
        acc[val.winner][val.season] = 1;
      }

    }
    //console.log(acc)
    return acc;
  }, {})

  return matchesWon;

};

// Q3

const getExtraRunsPerTeamForYear = (matches, deliveries) => {

  var match_id = matches.filter(val => val.season === '2016').map(item => item.id);


  var extraRuns = deliveries.reduce((acc, val) => {
    if (match_id.includes(val['match_id'])) {
      if (acc.hasOwnProperty(val['bowling_team'])) {
        acc[val['bowling_team']] += parseInt(val['extra_runs']);
      } else {
        acc[val['bowling_team']] = parseInt(val['extra_runs']);
      }
    }
    return acc;
  }, {})

  
  return extraRuns;
  //console.log(extraRuns);
};

// Q4

const getEconomicalBowlersForYear = (matches, deliveries) => {

  var match_id = matches.filter(val => val.season === '2015').map(item => item.id);

  function forBowlerOvers(match_id, deliveries) {

    var bowlersOvers = deliveries.reduce((acc, val) => {
      if (match_id.includes(val['match_id'])) {
        if (acc.hasOwnProperty(val['bowler'])) {
          acc[val['bowler']]['runs'] += parseInt(val['total_runs']);
          if (val['wide_runs'] == 0 && val['noball_runs'] == 0) {
            //console.log(val['ball']);
            acc[val['bowler']]['balls']++;
          }
        } else {
          acc[val['bowler']] = {};
          acc[val['bowler']]['runs'] = parseInt(val['total_runs']);
          acc[val['bowler']]['balls'] = 1;
        }



      }

      return acc;
    }, {})

    return forEconomyCalc(bowlersOvers);

  }




  function forEconomyCalc(bowlersOvers) {

    var econBowlers = [];
    for (const key in bowlersOvers) {
      //console.log(key);

      econBowlers.push([key, (bowlersOvers[key]['runs'] / (bowlersOvers[key]['balls'] / 6)).toFixed(2)]);
    }
    //console.log(econBowlers);
    return forSortingPlayers(econBowlers);
  }


  function forSortingPlayers(econBowlers) {

    econBowlers.sort((a, b) => a[1] - b[1]);
    var topTen = econBowlers.slice(0, 10);
    var topTenEco = {};
    for (var i in topTen) {
      topTenEco[topTen[i][0]] = topTen[i][1];
    }


    //console.log(topTenEco);
    return topTenEco;
  }
  return forBowlerOvers(match_id, deliveries);
};


module.exports = {
  getNoOfMatchesPlayed,
  getNoOfMatchesWonPerTeamPerYear,
  getExtraRunsPerTeamForYear,
  getEconomicalBowlersForYear
}